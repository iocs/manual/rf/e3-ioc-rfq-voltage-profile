require essioc

epicsEnvSet("ENGINEER", "Gabriel Fedel <gabriel.fedel@ess.eu>")
## Add extra environment variables here
epicsEnvSet("TOP",      "$(E3_CMD_TOP)")


iocshLoad("$(essioc_DIR)/common_config.iocsh")

require rfqvoltprof
dbLoadTemplate("rfqvoltprof.substitutions","dev=RFQ-010:EMR-Cav-001:,prefix=VoltProf,debug=0,asize=477")
iocInit()
